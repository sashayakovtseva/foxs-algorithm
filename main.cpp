#include <iostream>
#include <mpi.h>
#include <fstream>
#include <cmath>
#include <iomanip>

std::ofstream out;

void read_matrix(char *filename, int *&M, int &n, int &m) {
    std::ifstream in(filename);

    in >> n >> m;
    M = new int[n * m];
    for (int i = 0; i < n * m; ++i) {
        in >> M[i];
    }

    in.close();
}

void write_matrix(char *filename, int *M, int &n, int &m) {
    std::ofstream out(filename);

    out << n << " " <<  m << "\n";
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            out << M[i*m + j] << " ";
        }
        out << "\n";
    }

    out.close();
}

void print_matrix(int *v, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            std::cout << std::setw(10) << v[i * m + j];
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

void print_vector(std::ostream &out, int *v, int n) {
    if (v == nullptr) {
        out << "nullptr\n";
        return;
    }
    for (int i = 0; i < n; ++i) {
        out << std::setw(10) << v[i];
    }
    out << "\n\n";
}

void print_matrix(std::ostream &out, int *v, int n, int m) {
    if (v == nullptr) {
        out << "nullptr\n";
        return;
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            out << std::setw(10) << v[i * m + j];
        }
        out << "\n";
    }
    out << "\n";
}

void free_vector(const int *v) {
    delete[] v;
}

void distributeMatrixBlocks(int *rows, int n, int q, const int *sizes_h, MPI_Comm row_comm, int row_num, int *recvbuf) {
    // get number of columns in rows
    int m = 0;
    for (int i = 0; i < q; ++i) {
        m += sizes_h[i];
    }

    // process's rank in row comm is his column in cart
    int rank;
    MPI_Comm_rank(row_comm, &rank);

    // create new type for block's column
    MPI_Datatype block_col;
    MPI_Type_vector(n, 1, m, MPI_INT, &block_col);
    MPI_Type_create_resized(block_col, 0, sizeof(int), &block_col);
    MPI_Type_commit(&block_col);

    MPI_Datatype block_col_store;
    MPI_Type_vector(n, 1, sizes_h[rank], MPI_INT, &block_col_store);
    MPI_Type_create_resized(block_col_store, 0, sizeof(int), &block_col_store);
    MPI_Type_commit(&block_col_store);

    // correct displacements
    auto *displs = new int[q];
    displs[0] = 0;
    for (int i = 1; i < q; ++i) {
        displs[i] = displs[i - 1] + sizes_h[i - 1];
    }


    // distribute blocks
    // diagonal process has rows and rank of row number
    MPI_Scatterv(rows, sizes_h, displs, block_col, recvbuf, sizes_h[rank], block_col_store, row_num, row_comm);

    // free resources
    MPI_Type_free(&block_col);
    MPI_Type_free(&block_col_store);
    delete[]displs;
}

void distributeMatrixRows(int *M, int m, int q, const int *sizes_v, MPI_Comm cart_comm, int *recvbuf) {
    // define new data type of matrix to to send to diagonal processes
    MPI_Datatype matrix_row;
    MPI_Type_contiguous(m, MPI_INT, &matrix_row);
    MPI_Type_commit(&matrix_row);

    // correct displacements to select matrix's rows
    auto *displs = new int[q];
    displs[0] = 0;
    for (int i = 1; i < q; ++i) {
        displs[i] = displs[i - 1] + sizes_v[i - 1];
    }

    // get ranks of diagonal processes only
    auto *diag_ranks = new int[q];
    int coords[2];
    for (int i = 0; i < q; ++i) {
        coords[0] = i;
        coords[1] = i;
        MPI_Cart_rank(cart_comm, coords, &diag_ranks[i]);
    }

    int temp[2];
    int rank;
    MPI_Comm_rank(cart_comm, &rank);
    MPI_Cart_coords(cart_comm, rank, 2, temp);

    // create new communicator for diagonal processes only
    MPI_Group cart_group;
    MPI_Group diag_group;
    MPI_Comm diag_comm;
    MPI_Comm_group(cart_comm, &cart_group);
    MPI_Group_incl(cart_group, q, diag_ranks, &diag_group);
    MPI_Comm_create(cart_comm, diag_group, &diag_comm);

    if (diag_comm != MPI_COMM_NULL) {
        // distribute matrix rows
        // process {0, 0} has M
        MPI_Scatterv(M, sizes_v, displs, matrix_row, recvbuf, sizes_v[temp[0]], matrix_row, 0, diag_comm);
        MPI_Comm_free(&diag_comm);
    }

    // free resources
    MPI_Group_free(&cart_group);
    MPI_Group_free(&diag_group);
    MPI_Type_free(&matrix_row);
    free_vector(displs);
    free_vector(diag_ranks);
}

void distributeMatrix(int *M, int q, const int *sizes_v, const int *sizes_h, MPI_Comm cart_comm, int *recvbuf) {
    // get number of columns in M
    int m = 0;
    for (int i = 0; i < q; ++i) {
        m += sizes_h[i];
    }

    // get process's coordinates in cart communicator
    int coords[2];
    int rank;
    MPI_Comm_rank(cart_comm, &rank);
    MPI_Cart_coords(cart_comm, rank, 2, coords);

    // allocate memory for rows storing
    // only if process is diagonal
    int *rows = nullptr;
    if (coords[0] == coords[1]) {
        rows = new int[sizes_v[coords[0]] * m];
    }

    // distribute M's rows to diagonal processes
    distributeMatrixRows(M, m, q, sizes_v, cart_comm, rows);

    // create new communicator for cart's rows
    int dims[]{0, 1};
    MPI_Comm row_comm;
    MPI_Cart_sub(cart_comm, dims, &row_comm);

    // each diagonal process will distribute blocks in his row
    distributeMatrixBlocks(rows, sizes_v[coords[0]], q, sizes_h, row_comm, coords[0], recvbuf);

    // free resources
    free_vector(rows);
    MPI_Comm_free(&row_comm);
}

void gatherMatrixBlocks(int *block, int n, int q, const int *sizes_h, MPI_Comm row_comm, int row_num, int *rows) {
    // get number of columns in rows
    int m = 0;
    for (int i = 0; i < q; ++i) {
        m += sizes_h[i];
    }

    // process's rank in row comm is his column in cart
    int rank;
    MPI_Comm_rank(row_comm, &rank);

    MPI_Datatype block_col_store;
    MPI_Type_vector(n, 1, sizes_h[rank], MPI_INT, &block_col_store);
    MPI_Type_create_resized(block_col_store, 0, sizeof(int), &block_col_store);
    MPI_Type_commit(&block_col_store);


    // create new type for block's column
    MPI_Datatype block_col;
    MPI_Type_vector(n, 1, m, MPI_INT, &block_col);
    MPI_Type_create_resized(block_col, 0, sizeof(int), &block_col);
    MPI_Type_commit(&block_col);

    // correct displacements
    auto *displs = new int[q];
    displs[0] = 0;
    for (int i = 1; i < q; ++i) {
        displs[i] = displs[i - 1] + sizes_h[i - 1];
    }

    // gather blocks
    // diagonal process has rows and rank of row number
    MPI_Gatherv(block, sizes_h[rank], block_col_store, rows, sizes_h, displs, block_col, row_num, row_comm);

    // free resources
    MPI_Type_free(&block_col);
    MPI_Type_free(&block_col_store);
    delete[]displs;
}

void gatherMatrixRows(int *rows, int m, int q, const int *sizes_v, MPI_Comm cart_comm, int *M) {
    // define new data type of matrix row to to send to diagonal processes
    MPI_Datatype matrix_row;
    MPI_Type_contiguous(m, MPI_INT, &matrix_row);
    MPI_Type_commit(&matrix_row);

    // correct displacements to select matrix's rows
    auto *displs = new int[q];
    displs[0] = 0;
    for (int i = 1; i < q; ++i) {
        displs[i] = displs[i - 1] + sizes_v[i - 1];
    }

    // get ranks of diagonal processes only
    auto *diag_ranks = new int[q];
    int coords[2];
    for (int i = 0; i < q; ++i) {
        coords[0] = i;
        coords[1] = i;
        MPI_Cart_rank(cart_comm, coords, &diag_ranks[i]);
    }

    int temp[2];
    int rank;
    MPI_Comm_rank(cart_comm, &rank);
    MPI_Cart_coords(cart_comm, rank, 2, temp);


    // create new communicator for diagonal processes only
    MPI_Group cart_group;
    MPI_Group diag_group;
    MPI_Comm diag_comm;
    MPI_Comm_group(cart_comm, &cart_group);
    MPI_Group_incl(cart_group, q, diag_ranks, &diag_group);
    MPI_Comm_create(cart_comm, diag_group, &diag_comm);

    if (diag_comm != MPI_COMM_NULL) {
        // gather matrix rows
        // process {0, 0} has rows
        MPI_Gatherv(rows, sizes_v[temp[0]], matrix_row, M, sizes_v, displs, matrix_row, 0, diag_comm);
        MPI_Comm_free(&diag_comm);
    }

    // free resources
    MPI_Group_free(&cart_group);
    MPI_Group_free(&diag_group);
    MPI_Type_free(&matrix_row);
    free_vector(displs);
    free_vector(diag_ranks);
}

void gatherMatrix(int *M, int q, const int *sizes_v, const int *sizes_h, MPI_Comm cart_comm, int *block) {
    // get number of columns in M
    int m = 0;
    for (int i = 0; i < q; ++i) {
        m += sizes_h[i];
    }

    // get process's coordinates in cart communicator
    int coords[2];
    int rank;
    MPI_Comm_rank(cart_comm, &rank);
    MPI_Cart_coords(cart_comm, rank, 2, coords);

    // allocate memory for rows storing
    // only if process is diagonal
    int *rows = nullptr;
    if (coords[0] == coords[1]) {
        rows = new int[sizes_v[coords[0]] * m];
    }


    // create new communicator for cart's rows
    int dims[]{0, 1};
    MPI_Comm row_comm;
    MPI_Cart_sub(cart_comm, dims, &row_comm);

    // gather M's rows to diagonal processes
    gatherMatrixBlocks(block, sizes_v[coords[0]], q, sizes_h, row_comm, coords[0], rows);

    // each diagonal process will distribute blocks in his row
    gatherMatrixRows(rows, m, q, sizes_v, cart_comm, M);

    // free resources
    free_vector(rows);
    MPI_Comm_free(&row_comm);
}

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);

    int p; // number of processes
    int q; // square root of p
    int rank; // process's rank in WORLD
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    q = (int) sqrt(p);
    if (q * q != p) {
        if (rank == 0) {
            std::cout << "Number of processes is not an exact square. Aborting...\n";
        }
        MPI_Finalize();
        return 0;
    }


    bool is_cart_root = false; // is this process with {0, 0} coordinates?
    int cart_rank; // process's rank in cart
    int coords[2]; // process's coordinates in cart
    int cart_dims[]{q, q}; // cart size is q x q
    int cart_periods[]{1, 0}; // connect first dimension for Bij shifts
    MPI_Comm cart_comm; // handler to cart communicator
    MPI_Group cart_group; // handler to underlying cart group
    MPI_Cart_create(MPI_COMM_WORLD, 2, cart_dims, cart_periods, 1, &cart_comm);
    MPI_Comm_group(cart_comm, &cart_group);
    MPI_Comm_rank(cart_comm, &cart_rank);
    MPI_Cart_coords(cart_comm, cart_rank, 2, coords);
    if (coords[0] == coords[1] && coords[0] == 0) {
        is_cart_root = true;
    }

    // logging!!!
    char logfile[20];
    sprintf(logfile, "log%d.txt", rank);
    out = std::ofstream(logfile);
    out << "My world rank " << rank << "\n";
    out << "My cart rank " << cart_rank << "\n";
    out << "My coordinates " << coords[0] << " " << coords[1] << "\n";


    int *A = nullptr;
    int *B = nullptr;
    int n_A, m_A;
    int n_B, m_B;
    if (is_cart_root) {
        // read input matrices in main process
        read_matrix((char *) "A.txt", A, n_A, m_A);
        read_matrix((char *) "B.txt", B, n_B, m_B);
    }
    // send matrices' size to everyone
    MPI_Bcast(&n_A, 1, MPI_INT, 0, cart_comm);
    MPI_Bcast(&m_A, 1, MPI_INT, 0, cart_comm);
    MPI_Bcast(&n_B, 1, MPI_INT, 0, cart_comm);
    MPI_Bcast(&m_B, 1, MPI_INT, 0, cart_comm);
    if (m_A != n_B) {
        if (is_cart_root) {
            std::cout << "Incompatible matrices!\n";
            std::cout << "Aborting...\n";
            free_vector(A);
            free_vector(B);
        }
        MPI_Finalize();
        return 0;
    }
    if (n_A / q == 0 || m_A /q == 0 || n_B /q ==0 || m_B /q == 0) {
        if (is_cart_root) {
            std::cout << "Too many processes for such matrices!\n";
            std::cout << "Aborting...\n";
            free_vector(A);
            free_vector(B);
        }
        MPI_Finalize();
        return 0;
    }

    // here we will store multiplication result
    int *C = nullptr;
    if (is_cart_root) {
        C = new int[n_A * m_B];
    }


    // sizes of matrix blocks algorithm will operate on
    auto *block_sizes_A_vert = new int[q];
    auto *block_sizes_A_hor = new int[q];
    auto *block_sizes_B_vert = new int[q];
    auto *block_sizes_B_hor = new int[q];
    for (int i = 0; i < q; ++i) {
        block_sizes_A_vert[i] = n_A / q + (int) (i < n_A % q);
        block_sizes_A_hor[i] = m_A / q + (int) (i < m_A % q);
        block_sizes_B_vert[i] = n_B / q + (int) (i < n_B % q);
        block_sizes_B_hor[i] = m_B / q + (int) (i < m_B % q);
    }

    if (is_cart_root) {
        print_vector(out, block_sizes_A_vert, q);
        print_vector(out, block_sizes_A_hor, q);
        print_vector(out, block_sizes_B_vert, q);
        print_vector(out, block_sizes_B_hor, q);
    }


    int max_A_block_size = block_sizes_A_vert[coords[0]] * block_sizes_A_hor[0];
    int max_B_block_size = block_sizes_B_vert[0] * block_sizes_B_hor[coords[1]];
    int n_c = block_sizes_A_vert[coords[0]];
    int m_c = block_sizes_B_hor[coords[1]];
    int result_C_block_size = n_c * m_c;
    auto *start_block_A = new int[max_A_block_size];
    auto *recv_block_A = new int[max_A_block_size];
    auto *next_block_A = new int[max_A_block_size];
    auto *start_block_B = new int[max_B_block_size];
    auto *next_block_B = new int[max_B_block_size];
    auto *C_block = new int[result_C_block_size];
    int *work_block_A[]{recv_block_A, next_block_A};
    int *work_block_B[]{start_block_B, next_block_B};


    // at the end of the algorithm each process will hold
    // exactly one block of resulting C matrix
    // let's zero it before work
    std::fill(C_block, C_block + result_C_block_size, 0);

    distributeMatrix(A, q, block_sizes_A_vert, block_sizes_A_hor, cart_comm, start_block_A);
    out << "My start A block:\n";
    print_matrix(out, start_block_A, block_sizes_A_vert[coords[0]], block_sizes_A_hor[coords[1]]);
    out << "My start A block as array:\n";
    print_vector(out, start_block_A, max_A_block_size);

    distributeMatrix(B, q, block_sizes_B_vert, block_sizes_B_hor, cart_comm, start_block_B);
    out << "My start B block:\n";
    print_matrix(out, start_block_B, block_sizes_B_vert[coords[0]], block_sizes_B_hor[coords[1]]);
    out << "My start B block as array:\n";
    print_vector(out, start_block_B, max_B_block_size);


    int dims[]{0, 1};
    MPI_Comm row_comm;
    MPI_Group row_group;
    MPI_Cart_sub(cart_comm, dims, &row_comm); // create row communicators for Aik distributing
    MPI_Comm_group(row_comm, &row_group); // create group for further rank translation

    // send first Aik in blocking mode
    if (coords[0] == coords[1]) {
        out << "\n\nI will send A block before first step\n";
        work_block_A[0] = start_block_A;
    }
    MPI_Bcast(work_block_A[0], block_sizes_A_vert[coords[0]] * block_sizes_A_hor[coords[0]], MPI_INT, coords[0],
              row_comm);
    out << "\n\nMy A block before first step:\n";
    print_matrix(out, work_block_A[0], block_sizes_A_vert[coords[0]], block_sizes_A_hor[coords[0]]);
    out << "\n\nMy A block before first step array\n";
    print_vector(out, work_block_A[0], max_A_block_size);

    int recv_from, send_to; // ranks of processes in shift operation
    MPI_Request req_send = MPI_REQUEST_NULL;
    MPI_Request req_recv = MPI_REQUEST_NULL;
    MPI_Request req_bcast = MPI_REQUEST_NULL;
    MPI_Cart_shift(cart_comm, 0, 1, &recv_from, &send_to);

    // algorithm
    for (int step = 0, c = 0, i = coords[0], j = coords[1]; step < q; ++step, c ^= 1) {
        if (step > 0) {
            out << "Waiting send req\n";
            MPI_Wait(&req_send, MPI_STATUS_IGNORE);
            out << "Waiting recv req\n";
            MPI_Wait(&req_recv, MPI_STATUS_IGNORE);
            out << "Waiting bcast req\n";
            MPI_Wait(&req_bcast, MPI_STATUS_IGNORE);
        }

        int k = (i + step + 1) % q;
        if (k == j) {
            work_block_A[c ^ 1] = start_block_A;
        }

        if (step + 1 < q) {
            out << "Initiating send of B block to " << send_to << " and receiving from " << recv_from << "\n";
            MPI_Isend(work_block_B[c],
                      block_sizes_B_vert[(i + step) % q] * block_sizes_B_hor[j],
                      MPI_INT, send_to, 666, cart_comm, &req_send);
            MPI_Irecv(work_block_B[c ^ 1], block_sizes_B_vert[(i + 1 + step) % q] * block_sizes_B_hor[j],
                      MPI_INT, recv_from, 666, cart_comm, &req_recv);
            out << "Initiating bcast of next Aik\n";
            MPI_Ibcast(work_block_A[c ^ 1], block_sizes_A_vert[i] * block_sizes_A_hor[(i + step + 1) % q],
                       MPI_INT, k, row_comm, &req_bcast);
        }


        int n_a = block_sizes_A_vert[i];
        int m_a = block_sizes_A_hor[(i + step) % q];
        int n_b = block_sizes_B_vert[(i + step) % q];
        int m_b = block_sizes_B_hor[j];


        out << "\nWorking A block:\n";
        print_matrix(out, work_block_A[c], n_a, m_a);
        out << "\nWorking B block:\n";
        print_matrix(out, work_block_B[c], n_b, m_b);


        for (int l = 0; l < n_a; ++l) {
            for (int m = 0; m < m_b; ++m) {
                for (int s = 0; s < m_a; ++s) {
                    C_block[l * m_c + m] += work_block_A[c][l * m_a + s] * work_block_B[c][s * m_b + m];
                }
            }
        }

        out << "\n\nMy C_block after step #" << step << "\n";
        print_matrix(out, C_block, n_c, m_c);

        // don't forget to set correct working block A
        // pointers for process that broadcasted Aik
        if (work_block_A[c] == start_block_A) {
            work_block_A[c] = c == 0 ? recv_block_A : next_block_A;
        }
    }

    if (req_recv != MPI_REQUEST_NULL)
        MPI_Request_free(&req_recv);
    if (req_send != MPI_REQUEST_NULL)
        MPI_Request_free(&req_send);
    if (req_bcast != MPI_REQUEST_NULL)
        MPI_Request_free(&req_bcast);


    gatherMatrix(C, q, block_sizes_A_vert, block_sizes_B_hor, cart_comm, C_block);
    if (is_cart_root) {
        write_matrix((char *)"C.txt", C, n_A, m_B);
        free_vector(A);
        free_vector(B);
        free_vector(C);
    }

    free_vector(C_block);
    free_vector(start_block_A);
    free_vector(recv_block_A);
    free_vector(next_block_A);
    free_vector(start_block_B);
    free_vector(next_block_B);

    free_vector(block_sizes_B_vert);
    free_vector(block_sizes_B_hor);
    free_vector(block_sizes_A_vert);
    free_vector(block_sizes_A_hor);


    MPI_Comm_free(&cart_comm);
    MPI_Group_free(&cart_group);
    MPI_Comm_free(&row_comm);
    MPI_Group_free(&row_group);

    out.close();
    MPI_Finalize();
    return 0;
}
