build:
	mpic++ -o main -std=c++11 main.cpp

clean:
	rm main

run:
	mpirun -n ${N} ./main
